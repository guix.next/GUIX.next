;; Designed to be sourced by init.el or it's eqvivalent

;;; File managing emacs hooks

;; elisp
(add-hook 'emacs-lisp-mode-hook #'display-line-numbers-mode)
(add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)
(add-hook 'emacs-lisp-mode-hook #'whitespace-mode)

;; Scheme
(add-to-list 'auto-mode-alist '("\\.mgix\\'" . yaml-mode))
(add-hook 'scheme-mode-hook #'display-line-numbers-mode)
(add-hook 'scheme-mode-hook #'rainbow-delimiters-mode)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook #'whitespace-mode)
;; Scheme - GNU Guile
(add-hook 'scheme-mode-hook #'guix-devel-mode)

;; Yaml
(add-to-list 'auto-mode-alist '("\\.wpci\\'" . yaml-mode))
(add-hook 'yaml-mode-hook #'display-line-numbers-mode)

;; Dockerfiles
(add-hook 'dockerfile-mode-hook #'display-line-numbers-mode)
