;;; KEYBINDS
;; Treemacs
(define-key global-map (kbd "C-x C-\\") 'treemacs)
;; Terminal
(define-key global-map (kbd "C-`") 'ansi-term)
;; Splitting
(define-key global-map (kbd "<M-up>") 'split-window-vertically)
(define-key global-map (kbd "<M-down>") 'split-window-vertically)
(define-key global-map (kbd "<M-left>") 'split-window-horizontally)
(define-key global-map (kbd "<M-right>") 'split-window-right)
;; Quit window
(define-key global-map (kbd "C-q") 'delete-window)
