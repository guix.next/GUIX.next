;;; ENVIRONMENT
;; Enable CUA mode for cut, copy, paste by default
(cua-mode t)
;; FIXME(Krey): Implement logic to ask the end-user whether they want to enable evil-mode on startup
(evil-mode 1)
